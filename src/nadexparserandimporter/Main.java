/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nadexparserandimporter;

import com.granberg.nadexparserimporter.csv.parser.ForexParserImporter;
import com.granberg.nadexparserimporter.importer.HourlyPipsImporter;
import com.granberg.nadexparserimporter.controller.ControllerFactory;
import com.granberg.nadexparserimporter.loader.MYSQLDBAccess;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mdayacap
 */
public class Main {

    private static final String APP_PROP_PATH = "./app.properties";
    private static final String FOREX_TIMESTAMP_FORMAT = "forex.format.timestamp";
    private static final String URL = "db.url";
    private static final String DRIVER = "db.driver";
    private static final String DUMP_DIR = "dir.dump";
    private static final String HOURLYPIPS_INSTRUMENT_NAME = "hourlypips.instrumentname";
    private static final String HOURLYPIPS_START_DATE = "hourlypips.startdate";
    private static final String HOURLYPIPS_END_DATE = "hourlypips.enddate";
    private static final String HOURLYPIPS_AVE_TYPE = "hourlypips.averagetype";
    private static final String TARGET_HOUR = "hourlypips.targethour"; 
    
    private static final Properties app;
    
    static{
        app = new Properties();
        try {
            app.load(new FileInputStream(APP_PROP_PATH));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void main(String[] args) throws ParseException {
//        importForexCSV();
        importHourlyPips();
    }

    public static void importForexCSV() {

        MYSQLDBAccess loader = MYSQLDBAccess.getInstance();
        loader.setUrl(app.getProperty(URL));
        loader.setDriver(app.getProperty(DRIVER));
        loader.connect();
        ForexParserImporter parser = new ForexParserImporter(app.getProperty(DUMP_DIR), loader, app.getProperty(FOREX_TIMESTAMP_FORMAT));
        parser.importToDB();

    }
    
    public static void importHourlyPips() throws ParseException{
        ControllerFactory cf = ControllerFactory.getControllerFactory(ControllerFactory.MYSQL);
        HourlyPipsImporter importer = new HourlyPipsImporter(cf.getForexJpaController(), cf.getHourlyPipsJpaController());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String instrumentName = app.getProperty(HOURLYPIPS_INSTRUMENT_NAME);
        Date startDate = df.parse(app.getProperty(HOURLYPIPS_START_DATE));
        Date endDate = df.parse(app.getProperty(HOURLYPIPS_END_DATE));
        int averageType = Integer.parseInt(app.getProperty(HOURLYPIPS_AVE_TYPE));
        int targetHour = Integer.parseInt(app.getProperty(TARGET_HOUR));
        importer.importHourlyPips(instrumentName, endDate, targetHour, averageType);
    }
}
